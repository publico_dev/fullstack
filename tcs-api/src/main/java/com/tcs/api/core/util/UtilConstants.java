package com.tcs.api.core.util;

/**
 * Clase que contiene los valores constantes usados transversalmente en la aplicacion
 * 
 * @author omar.cardona
 *
 * @version 1.0
 */
public class UtilConstants {
	// Mensajes
	public static final String LINK_MORE_INFO = "https://www.tcs.com/moreInformation";
	public static final String MSG_TRANSACTION_OK = "Transacci�n exitosa.";
	public static final String MSG_TRANSACTION_ERROR = "Transacci�n erronea.";
	
	// Mensaje de excepciones
	public static final String EXCEP_GRAL_USR = "Los datos son inv�lidos, por favor verifique.";
	public static final String EXCEP_GRAL_DEV = "Los datos recibidos en el body de la peticion son incorrectos.";
	public static final String EXCEP_FORMAT_EMAIL_USR = "El correo electr�nico no cumple con el formato.";
	public static final String EXCEP_FORMAT_EMAIL_DEV = "Error en formato del atributo: empCorreo.";
	public static final String EXCEP_FORMAT_NUMBERDOC_USR = "El n�mero de documento no cumple con el formato.";
	public static final String EXCEP_FORMAT_NUMBERDOC_DEV = "Error en formato del atributo: empNumeroDocumento.";
	public static final String EXCEP_FORMAT_CELLPHONE_USR = "El n�mero de celular no cumple con el formato.";
	public static final String EXCEP_FORMAT_CELLPHONE_DEV = "Error en formato del atributo: empTelefono.";
	
	// Regex
    public static final String EMAIL_REGEX = "^[\\w!#$%&�*+/=?`{|}~^-]+(?:\\.[\\w!#$%&�*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
    public static final String CELLPHONE_REGEX = "^[1-9]{2,3}[0-9]{8,14}$";
    public static final String NUMBERDOC_REGEX = "^[0-9]{6,11}$";
	

}