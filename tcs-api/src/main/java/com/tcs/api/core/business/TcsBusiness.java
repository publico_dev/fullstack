package com.tcs.api.core.business;

import com.tcs.api.core.domain.EmpleadoDTO;
import com.tcs.api.core.domain.response.Response;

/**
 * Interface que permite definir las operaciones de negocio de Empleado
 * 
 * @author omar.cardona
 *
 * @version 1.0
 */
public interface TcsBusiness {
	
	/**
	 * M�todo que permite consultar un Empleado por su n�mero de documento
	 * 
	 * @param numeroDocumento N�mero de documento para realizar la consulta del empleado
	 * @return {@link Response} Objeto de respuesta para el servicio, el cual contiene la informaci�n sobre el resultado de la transacci�n
	 */
	Response<Void> empleado(String numeroDocumento);
	
	/**
	 * M�todo que permite consultar la lista de Empleados
	 * 
	 * @param page Indice de la paginaci�n
	 * @param pageSize Cantidad de elementos por p�gina
	 * @param sort Nombre del atributo por el cual ser� ordenado
	 * @param sortDirection Direcci�n del ordenamiento (ASC, DES)
	 * @return {@link Response} Objeto de respuesta para el servicio, el cual contiene la informaci�n sobre el resultado de la transacci�n
	 */
	Response<Void> empleadoList(int page, int pageSize, String sort, String sortDirection);
	
	/**
	 * M�todo que permite crear un Empleado
	 * 
	 * @param {@link EmpleadoDTO} DTO con la informaci�n para realizar la creaci�n del empleado
	 * @return {@link Response} Objeto de respuesta para el servicio, el cual contiene la informaci�n sobre el resultado de la transacci�n
	 */
	Response<Void> empleadoCreate(EmpleadoDTO empleadoDTO);

	/**
	 * M�todo que permite actualizar un Empleado
	 * 
	 * @param {@link EmpleadoDTO} DTO con la informaci�n para realizar la actualizaci�n del empleado
	 * @return {@link Response} Objeto de respuesta para el servicio, el cual contiene la informaci�n sobre el resultado de la transacci�n
	 */
	Response<Void> empleadoUpdate(EmpleadoDTO empleadoDTO);

	/**
	 * M�todo que permite consultar la lista de Departamentos
	 * 
	 * @param page Indice de la paginaci�n
	 * @param pageSize Cantidad de elementos por p�gina
	 * @param sort Nombre del atributo por el cual ser� ordenado
	 * @param sortDirection Direcci�n del ordenamiento (ASC, DES)
	 * @return {@link Response} Objeto de respuesta para el servicio, el cual contiene la informaci�n sobre el resultado de la transacci�n
	 */
	Response<Void> departamentoList(int page, int pageSize, String sort, String sortDirection);

	/**
	 * M�todo que permite generar el reporte de promedio de salarios, agrupados por departamento
	 * 
	 * @return {@link Response} Objeto de respuesta para el servicio, el cual contiene la informaci�n sobre el resultado de la transacci�n
	 */
	Response<Void> departamentoReport();
	
	/**
	 * M�todo que permite consultar la lista de Funciones
	 * 
	 * @param page Indice de la paginaci�n
	 * @param pageSize Cantidad de elementos por p�gina
	 * @param sort Nombre del atributo por el cual ser� ordenado
	 * @param sortDirection Direcci�n del ordenamiento (ASC, DES)
	 * @return {@link Response} Objeto de respuesta para el servicio, el cual contiene la informaci�n sobre el resultado de la transacci�n
	 */
	Response<Void> funcionesList(int page, int pageSize, String sort, String sortDirection);

}
