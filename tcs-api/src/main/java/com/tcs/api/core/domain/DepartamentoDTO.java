package com.tcs.api.core.domain;

import java.io.Serializable;

/**
 * Clase que define un DTO para la transmisión de la información de entidades Departamento
 * 
 * @author omar.cardona
 * 
 * @version 1.0
 */
public class DepartamentoDTO implements Serializable {

	/** Serializes */
	private static final long serialVersionUID = 8625505425134295713L;

	/** Identificador del departamento */
	private Integer depId;
	
	/** Nombre del departamento */
	private String depNombre;
	
	/** Descripción del departamento */
	private String depDescripcion;
	
	/** Código del departamento */
	private Integer depCodigo;

	/**
	 * @return the depId
	 */
	public Integer getDepId() {
		return depId;
	}

	/**
	 * @param depId the depId to set
	 */
	public void setDepId(Integer depId) {
		this.depId = depId;
	}

	/**
	 * @return the depNombre
	 */
	public String getDepNombre() {
		return depNombre;
	}

	/**
	 * @param depNombre the depNombre to set
	 */
	public void setDepNombre(String depNombre) {
		this.depNombre = depNombre;
	}

	/**
	 * @return the depDescripcion
	 */
	public String getDepDescripcion() {
		return depDescripcion;
	}

	/**
	 * @param depDescripcion the depDescripcion to set
	 */
	public void setDepDescripcion(String depDescripcion) {
		this.depDescripcion = depDescripcion;
	}

	/**
	 * @return the depCodigo
	 */
	public Integer getDepCodigo() {
		return depCodigo;
	}

	/**
	 * @param depCodigo the depCodigo to set
	 */
	public void setDepCodigo(Integer depCodigo) {
		this.depCodigo = depCodigo;
	}

	@Override
	public String toString() {
		return "DepartamentoDTO [depId=" + depId + ", depNombre=" + depNombre + ", depDescripcion=" + depDescripcion
				+ ", depCodigo=" + depCodigo + ", depFunciones=" + "]";
	}

}
