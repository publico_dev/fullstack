package com.tcs.api.core.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.tcs.api.core.domain.DepartamentoDTO;
import com.tcs.api.core.model.Departamento;

/**
 * Clase que permite mapear entidad/DTO de Departamento
 * 
 * @author omar.cardona
 *
 * @version 1.0
 */
@Component
public class DepartamentoMapper {
	
//	/** Clase de mapeo para Empleado */
//    private EmpleadoMapper mapEmpleado;
//    
//    /** Clase de mapeo para Funciones */
//    private FuncionesMapper mapFunciones;
//    
//    /** Constructor
//     * 
//     * @param {@link EmpleadoMapper} Objeto de mapeo para Empleado
//     * @param {@link FuncionesMapper} Objeto de mapeo para Funciones
//     */
//    public DepartamentoMapper(EmpleadoMapper mapEmpleado, FuncionesMapper mapFunciones) {
//		this.mapEmpleado = mapEmpleado;
//		this.mapFunciones = mapFunciones;
//	}

    /**
     * Permite mapear un DTO de Departamento en la entidad correspondiente
     * 
     * @param {@link DepartamentoDTO} Objeto con la información de Departamento
     * @return Entidad con la informaciónn de Departamento
     */
    public Departamento fromDtoToEntity(DepartamentoDTO departamentoDTO) {
    	Departamento categoryApp = new Departamento();
    	categoryApp.setCodigo(departamentoDTO.getDepCodigo());
    	categoryApp.setDescripcion(departamentoDTO.getDepDescripcion());
    	categoryApp.setId(departamentoDTO.getDepId());
    	categoryApp.setNombre(departamentoDTO.getDepNombre());
    	
//    	// Set de empleados
//    	Set<EmpleadoDTO> arrEmpleadoDTO = departamentoDTO.getDepEmpleados();
//    	Set<Empleado> arrEmpleado = new HashSet<>();
//    	for( EmpleadoDTO empleadoDTO : arrEmpleadoDTO ) {
//			arrEmpleado.add( mapEmpleado.fromDtoToEntity(empleadoDTO) );
//    	}
//    	categoryApp.setEmpleados(arrEmpleado);
//    	
//    	// Set de funciones
//    	Set<FuncionesDTO> arrFuncionesDTO = departamentoDTO.getDepFunciones();
//    	Set<Funciones> arrFunciones = new HashSet<>();
//    	for( FuncionesDTO funcionesDTO : arrFuncionesDTO ) {
//			arrFunciones.add( mapFunciones.fromDtoToEntity(funcionesDTO) );
//    	}
//    	categoryApp.setFunciones(arrFunciones);
    	
        return categoryApp;
    }

    /**
     * Permite mapear una entidad de Departamento en el DTO correspondiente
     * 
     * @param {@link Departamento} Entidad con la información de Departamento
     * @return DTO con la información de Departamento
     */
    public DepartamentoDTO fromEntityToDto(Departamento departamento) {
    	DepartamentoDTO departamentoDTO = new DepartamentoDTO();
    	
        departamentoDTO.setDepCodigo(departamento.getCodigo());
        departamentoDTO.setDepDescripcion(departamento.getDescripcion());
        departamentoDTO.setDepId(departamento.getId());
        departamentoDTO.setDepNombre(departamento.getNombre());
        
//        // Set de empleados
//        Set<Empleado> arrEmpleado = departamento.getEmpleados();
//        Set<EmpleadoDTO> arrEmpleadoDTO = new HashSet<>();
//        for( Empleado empleado : arrEmpleado ) {
//			arrEmpleadoDTO.add( mapEmpleado.fromEntityToDto(empleado) );
//    	}
//        departamentoDTO.setDepEmpleados(arrEmpleadoDTO);
//        
//        // Set de funciones
//        Set<Funciones> arrFunciones = departamento.getFunciones();
//        Set<FuncionesDTO> arrFuncionesDTO = new HashSet<>();
//        for( Funciones funciones : arrFunciones ) {
//			arrFuncionesDTO.add( mapFunciones.fromEntityToDto(funciones) );
//    	}
//        departamentoDTO.setDepFunciones(arrFuncionesDTO);
        
        return departamentoDTO;
    }
    
    /**
     * Transforma un listado de DepartamentoDTO en un listado de entidades correspondientes
     * 
     * @param arrDepartamentoDTO Listado de DTOs DepartamentoDTO
     * @return Listado de entidades Empleado
     */
	public List<Departamento> fromDtosToEntities(List<DepartamentoDTO> arrDepartamentoDTO) {
		DepartamentoDTO departamentoDTO;
		List<Departamento> arrDepartamento  = new ArrayList<>();
		
		for(int i=0; i<arrDepartamentoDTO.size(); i++) {
			departamentoDTO = arrDepartamentoDTO.get(i);

			arrDepartamento.add(fromDtoToEntity(departamentoDTO) );
    	}
		return arrDepartamento;
	}
	
	/**
     * Transforma un listado de Departamento en un listado de DTOs correspondientes
     * 
     * @param arrDepartamento Listado de Entidades Departamento
     * @return Listado de DTOs DepartamentoDTO
     */
	public List<DepartamentoDTO> fromEntitiesToDtos(List<Departamento> arrDepartamento) {
		Departamento departamento;
		List<DepartamentoDTO> arrDepartamentoDTO  = new ArrayList<>();
		
		for(int i=0; i<arrDepartamento.size(); i++) {
			departamento = arrDepartamento.get(i);

			arrDepartamentoDTO.add(fromEntityToDto(departamento) );
    	}
		return arrDepartamentoDTO;
	}
}
