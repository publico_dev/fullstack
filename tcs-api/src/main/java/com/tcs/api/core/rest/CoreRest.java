package com.tcs.api.core.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.tcs.api.core.business.TcsBusiness;
import com.tcs.api.core.domain.EmpleadoDTO;
import com.tcs.api.core.domain.response.Response;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;

@RestController
@RequestMapping("/api/v1.0")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@SwaggerDefinition(tags = { @Tag(name = "general", description = "RestController para el servicio") })
public class CoreRest {
	
    @Autowired
    private TcsBusiness empleadoBusiness;
    
    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder){
        return builder.build();
    }
    
    
    @ApiOperation(value = "Permite consultar un empleado.", 
    		notes = "Consulta de empleado", 
    		produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Consulta de empleado exitosa", response = Response.class),
    @ApiResponse(code = 400, message = "Error en la consulta, debido a un error en los datos.", response = Response.class),
    @ApiResponse(code = 500, message = "Error en la consulta, generado por un error en el servidor", response = Response.class) })
    @GetMapping(value = "/empleado", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Response<Void> empleado(
    		@RequestParam(name = "numeroDocumento", required = true) String numeroDocumento) {
    	
    	return empleadoBusiness.empleado(numeroDocumento);
    }
    
    @ApiOperation(value = "Permite consultar los empleados.", 
    		notes = "Consulta de empleados", 
    		produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Consulta de empleados exitosa", response = Response.class),
    @ApiResponse(code = 400, message = "Error en la consulta, debido a un error en los datos.", response = Response.class),
    @ApiResponse(code = 500, message = "Error en la consulta, generado por un error en el servidor", response = Response.class) })
    @GetMapping(value = "/empleado/list", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Response<Void> empleadoList(
    		@RequestParam(name = "page", required = true) int page,
    		@RequestParam(name = "pageSize", required = true) int pageSize,
    		@RequestParam(defaultValue = "apellidos") String sort,
    		@RequestParam(defaultValue = "ASC") String sortDirection) {
    	
    	return empleadoBusiness.empleadoList(page, pageSize, sort, sortDirection);
    }
    
    @ApiOperation(value = "Permite crear un empleado.", 
    		notes = "Creación de empleado",
    		consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, 
    		produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Creación de empleado exitosa", response = Response.class),
    @ApiResponse(code = 400, message = "Error en la creación, debido a un error en los datos.", response = Response.class),
    @ApiResponse(code = 500, message = "Error en la creación, generado por un error en el servidor", response = Response.class) })
    @PostMapping(value = "/empleado/create", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Response<Void> empleadoCreate(
    		@ApiParam(name = "empleadoDTO", value = "JSON con los parámetros para realizar la creación de empleado", required = true, type = "EmpleadoDTO") @RequestBody EmpleadoDTO empleadoDTO) {
    	
    	return empleadoBusiness.empleadoCreate(empleadoDTO);
    }
    
    @ApiOperation(value = "Permite actualizar un empleado.", 
    		notes = "Actualización de empleado",
    		consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, 
    		produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Actualización de empleado exitosa", response = Response.class),
    @ApiResponse(code = 400, message = "Error en la actualización, debido a un error en los datos.", response = Response.class),
    @ApiResponse(code = 500, message = "Error en la actualización, generado por un error en el servidor", response = Response.class) })
    @PutMapping(value = "/empleado/update", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Response<Void> empleadoUpdate(
    		@ApiParam(name = "empleadoDTO", value = "JSON con los parámetros para realizar la actualización de empleado", required = true, type = "EmpleadoDTO") @RequestBody EmpleadoDTO empleadoDTO) {
    	
    	return empleadoBusiness.empleadoUpdate(empleadoDTO);
    }
    
    @ApiOperation(value = "Permite consultar los departamentos.", 
    		notes = "Consulta de departamentos", 
    		produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Consulta de departamentos exitosa", response = Response.class),
    @ApiResponse(code = 400, message = "Error en la consulta, debido a un error en los datos.", response = Response.class),
    @ApiResponse(code = 500, message = "Error en la consulta, generado por un error en el servidor", response = Response.class) })
    @GetMapping(value = "/departamento/list", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Response<Void> departamentoList(
    		@RequestParam(name = "page", required = true) int page,
    		@RequestParam(name = "pageSize", required = true) int pageSize,
    		@RequestParam(defaultValue = "apellidos") String sort,
    		@RequestParam(defaultValue = "ASC") String sortDirection) {
    	
    	return empleadoBusiness.departamentoList(page, pageSize, sort, sortDirection);
    }
    
    @ApiOperation(value = "Permite consultar el reporte de promedio de salarios por departamentos.", 
    		notes = "Consulta de reporte", 
    		produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Consulta de reporte exitosa", response = Response.class),
    @ApiResponse(code = 400, message = "Error en la consulta, debido a un error en los datos.", response = Response.class),
    @ApiResponse(code = 500, message = "Error en la consulta, generado por un error en el servidor", response = Response.class) })
    @GetMapping(value = "/departamento/report", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Response<Void> departamentoReport() {
    	
    	return empleadoBusiness.departamentoReport();
    }
    
    @ApiOperation(value = "Permite consultar las funciones.", 
    		notes = "Consulta de funciones", 
    		produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Consulta de funciones exitosa", response = Response.class),
    @ApiResponse(code = 400, message = "Error en la consulta, debido a un error en los datos.", response = Response.class),
    @ApiResponse(code = 500, message = "Error en la consulta, generado por un error en el servidor", response = Response.class) })
    @GetMapping(value = "/funcion/list", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Response<Void> funcionesList(
    		@RequestParam(name = "page", required = true) int page,
    		@RequestParam(name = "pageSize", required = true) int pageSize,
    		@RequestParam(defaultValue = "apellidos") String sort,
    		@RequestParam(defaultValue = "ASC") String sortDirection) {
    	
    	return empleadoBusiness.funcionesList(page, pageSize, sort, sortDirection);
    }
}
