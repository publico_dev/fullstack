package com.tcs.api.core.domain;

import java.io.Serializable;

/**
 * Clase que define un DTO para la transmisión de la información de reportes de promedio de salario por departamento
 * 
 * @author omar.cardona
 * 
 * @version 1.0
 */
public class ReportSalarioDTO implements Serializable {
	
	private static final long serialVersionUID = 3500156902569851025L;
	/** Salario promedio */
	private double promedio;
	
	/** Departamento */
    private DepartamentoDTO departamento;


	/**
	 * @return the promedio
	 */
	public double getPromedio() {
		return promedio;
	}

	/**
	 * @param promedio the promedio to set
	 */
	public void setPromedio(double promedio) {
		this.promedio = promedio;
	}

	/**
	 * @return the departamento
	 */
	public DepartamentoDTO getDepartamento() {
		return departamento;
	}

	/**
	 * @param departamento the departamento to set
	 */
	public void setDepartamento(DepartamentoDTO departamento) {
		this.departamento = departamento;
	}

	@Override
	public String toString() {
		return "ReportDepartamento [promedio=" + promedio + ", departamento="
				+ departamento + "]";
	}

}
