package com.tcs.api.core.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.tcs.api.core.domain.FuncionesDTO;
import com.tcs.api.core.model.Funciones;

/**
 * Clase que permite mapear entidad/DTO de Funciones
 * 
 * @author omar.cardona
 *
 * @version 1.0
 */
@Component
public class FuncionesMapper {
	
	/** Clase de mapeo para Departamento */
    private DepartamentoMapper mapDepartamento;
    
    /**
     * Constructor
     * 
     * @param {@link DepartamentoMapper} Objeto de mapeo para Departamento
     */
    public FuncionesMapper(DepartamentoMapper mapDepartamento) {
		this.mapDepartamento = mapDepartamento;
	}

    /**
     * Permite mapear un DTO de Funciones en la entidad correspondiente
     * 
     * @param {@link FuncionesDTO} Objeto con la información de Funciones
     * @return Entidad con la información de Funciones
     */
    public Funciones fromDtoToEntity(FuncionesDTO funcionesDTO) {
    	Funciones funciones = new Funciones();
    	funciones.setDescripcion(funcionesDTO.getFunDescripcion());
    	funciones.setId(funcionesDTO.getFunId());
    	funciones.setNombre(funcionesDTO.getFunNombre());

    	// Set de Departamento
    	funciones.setDepartamento(mapDepartamento.fromDtoToEntity(funcionesDTO.getFunDepartamento()));
    	
        return funciones;
    }

    /**
     * Permite mapear una entidad de Funciones en el DTO correspondiente
     * 
     * @param {@link Funciones} Entidad con la información de Funciones
     * @return DTO con la información de Funciones
     */
    public FuncionesDTO fromEntityToDto(Funciones funciones) {
    	FuncionesDTO funcionesDTO = new FuncionesDTO();
    	funcionesDTO.setFunDescripcion(funciones.getDescripcion());
    	funcionesDTO.setFunId(funciones.getId());
    	funcionesDTO.setFunNombre(funciones.getNombre());
    	
    	// Set de Departamento
    	funcionesDTO.setFunDepartamento(mapDepartamento.fromEntityToDto(funciones.getDepartamento()));
    	
        return funcionesDTO;
    }
    
    /**
     * Transforma un listado de FuncionesDTO en un listado de entidades correspondientes
     * 
     * @param arrFuncionesDTO Listado de DTOs FuncionesDTO
     * @return Listado de entidades Funciones
     */
	public List<Funciones> fromDtosToEntities(List<FuncionesDTO> arrFuncionesDTO) {
		FuncionesDTO funcionesDTO;
		List<Funciones> arrFunciones  = new ArrayList<>();
		
		for(int i=0; i<arrFuncionesDTO.size(); i++) {
			funcionesDTO = arrFuncionesDTO.get(i);

			arrFunciones.add(fromDtoToEntity(funcionesDTO) );
    	}
		return arrFunciones;
	}
	
	/**
     * Transforma un listado de Funciones en un listado de DTOs correspondientes
     * 
     * @param arrFunciones Listado de Entidades Funciones
     * @return Listado de DTOs FuncionesDTO
     */
	public List<FuncionesDTO> fromEntitiesToDtos(List<Funciones> arrFunciones) {
		Funciones funciones;
		List<FuncionesDTO> arrFuncionesDTO  = new ArrayList<>();
		
		for(int i=0; i<arrFunciones.size(); i++) {
			funciones = arrFunciones.get(i);

			arrFuncionesDTO.add(fromEntityToDto(funciones) );
    	}
		return arrFuncionesDTO;
	}

}
