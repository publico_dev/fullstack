package com.tcs.api.core.domain.response;

/**
 * Clase de respuesta paginada
 * 
 * @author omar.cardona
 *
 * @param <T> Objeto de respuesta para las transacciones realizadas en cada Api
 * 
 * @version 1.0
 * 
 */
public class ResponsePaginate<T> extends Response<T>{
	
	/** Limite de items para la respuesta paginada */
	private Integer pagLimit;
	
	/** Inidice de partida de resultados paginados */
	private Integer pagOffset;
	
	/**
	 * @return the pagLimit
	 */
	public Integer getPagLimit() {
		return pagLimit;
	}

	/**
	 * @param pagLimit the pagLimit to set
	 */
	public void setPagLimit(Integer pagLimit) {
		this.pagLimit = pagLimit;
	}

	/**
	 * @return the pagOffset
	 */
	public Integer getPagOffset() {
		return pagOffset;
	}

	/**
	 * @param pagOffset the pagOffset to set
	 */
	public void setPagOffset(Integer pagOffset) {
		this.pagOffset = pagOffset;
	}

	@Override
	public String toString() {
		return "ResponsePaginate [pagLimit=" + pagLimit + ", pagOffset=" + pagOffset
				+ ", toString()=" + super.toString() + "]";
	}

}
