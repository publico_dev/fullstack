package com.tcs.api.core.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.tcs.api.core.domain.EmpleadoDTO;
import com.tcs.api.core.domain.ReportSalarioDTO;
import com.tcs.api.core.model.Empleado;

/**
 * Clase que permite mapear entidad/DTO de Empleado
 * 
 * @author omar.cardona
 *
 * @version 1.0
 */
@Component
public class EmpleadoMapper {
	
	/** Clase de mapeo para Departamento */
    private DepartamentoMapper mapDepartamento;
    
    /**
     * Constructor
     * 
     * @param {@link DepartamentoMapper} Objeto de mapeo para Departamento
     */
    public EmpleadoMapper(DepartamentoMapper mapDepartamento) {
		this.mapDepartamento = mapDepartamento;
	}

    /**
     * Permite mapear un DTO de Empleado en la entidad correspondiente
     * 
     * @param {@link EmpleadoDTO} Objeto con la información de Empleado
     * @return Entidad con la información de Empleado
     */
    public Empleado fromDtoToEntity(EmpleadoDTO empleadoDTO) {
    	Empleado empleado = new Empleado();
    	empleado.setActivo(empleadoDTO.getEmpActivo());
    	empleado.setApellidos(empleadoDTO.getEmpApellidos());
    	empleado.setCorreo(empleadoDTO.getEmpCorreo());
    	empleado.setId(empleadoDTO.getEmpId());
    	empleado.setNombre(empleadoDTO.getEmpNombre());
    	empleado.setNumeroDocumento(empleadoDTO.getEmpNumeroDocumento());
    	empleado.setSalario(empleadoDTO.getEmpSalario());
    	empleado.setTelefono(empleadoDTO.getEmpTelefono());

    	// Set de Departamento
    	empleado.setDepartamento(mapDepartamento.fromDtoToEntity(empleadoDTO.getEmpDepartamento()));
    	
        return empleado;
    }

    /**
     * Permite mapear una entidad de Empleado en el DTO correspondiente
     * 
     * @param {@link Empleado} Entidad con la información de Empleado
     * @return DTO con la información de Empleado
     */
    public EmpleadoDTO fromEntityToDto(Empleado empleado) {
    	EmpleadoDTO empleadoDTO = new EmpleadoDTO();
    	empleadoDTO.setEmpActivo(empleado.getActivo());
    	empleadoDTO.setEmpApellidos(empleado.getApellidos());
    	empleadoDTO.setEmpCorreo(empleado.getCorreo());
    	empleadoDTO.setEmpId(empleado.getId());
    	empleadoDTO.setEmpNombre(empleado.getNombre());
    	empleadoDTO.setEmpNumeroDocumento(empleado.getNumeroDocumento());
    	empleadoDTO.setEmpSalario(empleado.getSalario());
    	empleadoDTO.setEmpTelefono(empleado.getTelefono());
    	
    	// Set de Departamento
    	empleadoDTO.setEmpDepartamento(mapDepartamento.fromEntityToDto(empleado.getDepartamento()));
    	
        return empleadoDTO;
    }
    
    /**
     * Transforma un listado de EmpleadoDTO en un listado de entidades correspondientes
     * 
     * @param arrEmpleadoDTO Listado de DTOs EmpleadoDTO
     * @return Listado de entidades Empleado
     */
	public List<Empleado> fromDtosToEntities(List<EmpleadoDTO> arrEmpleadoDTO) {
		EmpleadoDTO empleadoDTO;
		List<Empleado> arrEmpleado  = new ArrayList<>();
		
		for(int i=0; i<arrEmpleadoDTO.size(); i++) {
			empleadoDTO = arrEmpleadoDTO.get(i);

			arrEmpleado.add(fromDtoToEntity(empleadoDTO) );
    	}
		return arrEmpleado;
	}
	
	/**
     * Transforma un listado de Empleado en un listado de DTOs correspondientes
     * 
     * @param arrEmpleado Listado de Entidades Empleado
     * @return Listado de DTOs EmpleadoDTO
     */
	public List<EmpleadoDTO> fromEntitiesToDtos(List<Empleado> arrEmpleado) {
		Empleado empleado;
		List<EmpleadoDTO> arrEmpleadoDTO  = new ArrayList<>();
		
		for(int i=0; i<arrEmpleado.size(); i++) {
			empleado = arrEmpleado.get(i);

			arrEmpleadoDTO.add(fromEntityToDto(empleado) );
    	}
		return arrEmpleadoDTO;
	}
	
	/**
     * Permite mapear una entidad de Empleado en el DTO de ReportSalarioDTO
     * 
     * @param {@link Empleado} Entidad con la información de Empleado
     * @return DTO con la información de ReportSalarioDTO
     */
    public ReportSalarioDTO fromEntityToReporteSalario(Empleado empleado) {
    	ReportSalarioDTO reportSalarioDTO = new ReportSalarioDTO();
    	reportSalarioDTO.setDepartamento(mapDepartamento.fromEntityToDto(empleado.getDepartamento()));
    	reportSalarioDTO.setPromedio(empleado.getSalario());
    	
        return reportSalarioDTO;
    }
	
	/**
     * Transforma un listado de Empleado en un listado de DTOs de ReportSalarioDTO
     * 
     * @param arrEmpleado Listado de Entidades Empleado
     * @return Listado de DTOs ReportSalarioDTO
     */
	public List<ReportSalarioDTO> fromEntitiesToReport(List<Empleado> arrEmpleado) {
		Empleado empleado;
		List<ReportSalarioDTO> arrReportSalarioDTO  = new ArrayList<>();
		
		for(int i=0; i<arrEmpleado.size(); i++) {
			empleado = arrEmpleado.get(i);

			arrReportSalarioDTO.add(fromEntityToReporteSalario(empleado) );
    	}
		return arrReportSalarioDTO;
	}

}
