package com.tcs.api.core.domain;

import java.io.Serializable;

/**
 * Clase que define un DTO para la transmisi�n de la informaci�n de entidades Funciones
 * 
 * @author omar.cardona
 * 
 * @version 1.0
 */
public class FuncionesDTO implements Serializable {
	
	/** Serializes */
	private static final long serialVersionUID = 9166445716720236371L;
	
	/** Identificador de la funci�n */
	private Integer funId;
	
	/** Nombre de la funci�n */
	private String funNombre;
	
	/** Descripci�n de la funci�n */
	private String funDescripcion;
	
	/** Departamento al cual pertenece la funci�n */
    private DepartamentoDTO funDepartamento;

	/**
	 * @return the funId
	 */
	public Integer getFunId() {
		return funId;
	}

	/**
	 * @param funId the funId to set
	 */
	public void setFunId(Integer funId) {
		this.funId = funId;
	}

	/**
	 * @return the funNombre
	 */
	public String getFunNombre() {
		return funNombre;
	}

	/**
	 * @param funNombre the funNombre to set
	 */
	public void setFunNombre(String funNombre) {
		this.funNombre = funNombre;
	}

	/**
	 * @return the funDescripcion
	 */
	public String getFunDescripcion() {
		return funDescripcion;
	}

	/**
	 * @param funDescripcion the funDescripcion to set
	 */
	public void setFunDescripcion(String funDescripcion) {
		this.funDescripcion = funDescripcion;
	}

	/**
	 * @return the funDepartamento
	 */
	public DepartamentoDTO getFunDepartamento() {
		return funDepartamento;
	}

	/**
	 * @param funDepartamento the funDepartamento to set
	 */
	public void setFunDepartamento(DepartamentoDTO funDepartamento) {
		this.funDepartamento = funDepartamento;
	}

	@Override
	public String toString() {
		return "FuncionesDTO [funId=" + funId + ", funNombre=" + funNombre + ", funDescripcion=" + funDescripcion
				+ ", funDepartamento=" + funDepartamento + "]";
	}
    
}
