package com.tcs.api.core.util;


import static com.tcs.api.core.util.UtilConstants.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.tcs.api.core.domain.EmpleadoDTO;
import com.tcs.api.core.exception.CoreException;

/**
 * Clase donde se definen metodos utilitarios para validaciones generales
 * 
 * @author omar.cardona
 *
 * @version 1.0
 */
public class ValidatorUtil {

	/**
	 * Metodo que permite validar si un objeto es nulo
	 * 
	 * @param obj Objeto a comprobar
	 * @return true si es nulo, false en cualquier otro caso
	 */
	public static boolean isObjectNull(Object obj) {
		return obj == null;
	}

	/**
	 * Metodo que permite comprobar si una cadena es nula, vacia o solo contiene un
	 * espacio en blanco
	 * 
	 * @param text Cadena a validar
	 * @return true si la cadena es nula, vacia o solo contiene un espacio, false en
	 *         caso contrario
	 */
	public static boolean isNullOrEmpty(String text) {
		return text == null || text.trim().isEmpty();
	}

	/**
     * Permite verificar que un email cumpla con el formato esperado
     * 
     * @param email Email ingresado por el usuario
     * @return true en caso de que el email sea v�lido, false en caso contrario
     */
    public static boolean validateEmail(String email) {
        Pattern pattern = Pattern.compile(UtilConstants.EMAIL_REGEX);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
    
    /**
     * Permite verificar si un n�mero de celular cumple con el formato esperado
     * 
     * @param cellphone Numero de celular a verificar
     * @return true en caso de que el n�mero de celular sea v�lido, false en caso contrario
     */
    public static boolean isValidCellPhoneNumber(String cellphone) {
        Pattern pattern = Pattern.compile(UtilConstants.CELLPHONE_REGEX);
        Matcher matcher = pattern.matcher(cellphone);
        return matcher.matches();
    }
    
    /**
     * Permite verificar si un n�mero de documento cumple con el formato esperado
     * 
     * @param numberDoc Numero de documento a verificar
     * @return true en caso de que el n�mero de documento sea v�lido, false en caso contrario
     */
    public static boolean isValidNumberDocument(String numberDoc) {
        Pattern pattern = Pattern.compile(UtilConstants.NUMBERDOC_REGEX);
        Matcher matcher = pattern.matcher(numberDoc);
        return matcher.matches();
    }
    
	/**
     * M�todo que permite verificar la informaci�n del dto de EmpleadoDTO
     * @param empleadoDTO {@link EmpleadoDTO} Objeto con la informaci�n de Empleado
     * @throws CoreException En caso que la informaci�n no sea correcta
     */
    public static boolean verifyEmpleadoDTO(EmpleadoDTO empleadoDTO) throws CoreException {
        if (ValidatorUtil.isObjectNull(empleadoDTO) 
                || ValidatorUtil.isNullOrEmpty(empleadoDTO.getEmpApellidos())
                || ValidatorUtil.isNullOrEmpty(empleadoDTO.getEmpCorreo())
                || ValidatorUtil.isNullOrEmpty(empleadoDTO.getEmpNombre())
                || ValidatorUtil.isNullOrEmpty(empleadoDTO.getEmpNumeroDocumento())
                || ValidatorUtil.isNullOrEmpty(empleadoDTO.getEmpTelefono())
                || ValidatorUtil.isObjectNull(empleadoDTO.getEmpDepartamento())
                || ValidatorUtil.isObjectNull(empleadoDTO.getEmpSalario())) {
			throw new CoreException(EXCEP_GRAL_USR, EXCEP_GRAL_DEV, 400);
        } else if( !validateEmail(empleadoDTO.getEmpCorreo()) ) {
        	throw new CoreException(EXCEP_FORMAT_EMAIL_USR, EXCEP_FORMAT_EMAIL_DEV, 400);
        } else if( !isValidNumberDocument(empleadoDTO.getEmpNumeroDocumento()) ) {
        	throw new CoreException(EXCEP_FORMAT_NUMBERDOC_USR, EXCEP_FORMAT_NUMBERDOC_DEV, 400);
        } else if( !isValidCellPhoneNumber(empleadoDTO.getEmpTelefono()) ) {
        	throw new CoreException(EXCEP_FORMAT_CELLPHONE_USR, EXCEP_FORMAT_CELLPHONE_DEV, 400);
        }
        return true;
    }
    
    /**
     * M�todo que permite verificar la informaci�n del dto de EmpleadoDTO, para actualizaci�n
     * @param empleadoDTO {@link EmpleadoDTO} Objeto con la informaci�n de Empleado
     * @throws CoreException En caso que la informaci�n no sea correcta
     */
    public static boolean verifyEmpleadoDTOUpdate(EmpleadoDTO empleadoDTO) throws CoreException {
        if (ValidatorUtil.isObjectNull(empleadoDTO) 
                || ValidatorUtil.isNullOrEmpty(empleadoDTO.getEmpNumeroDocumento())) {
			throw new CoreException(EXCEP_GRAL_USR, EXCEP_GRAL_DEV, 400);
        } else if( !ValidatorUtil.isNullOrEmpty(empleadoDTO.getEmpCorreo()) && !validateEmail(empleadoDTO.getEmpCorreo()) ) {
        	throw new CoreException(EXCEP_FORMAT_EMAIL_USR, EXCEP_FORMAT_EMAIL_DEV, 400);
        } else if( !ValidatorUtil.isNullOrEmpty(empleadoDTO.getEmpNumeroDocumento()) && !isValidNumberDocument(empleadoDTO.getEmpNumeroDocumento()) ) {
        	throw new CoreException(EXCEP_FORMAT_NUMBERDOC_USR, EXCEP_FORMAT_NUMBERDOC_DEV, 400);
        } else if( !ValidatorUtil.isNullOrEmpty(empleadoDTO.getEmpTelefono()) && !isValidCellPhoneNumber(empleadoDTO.getEmpTelefono()) ) {
        	throw new CoreException(EXCEP_FORMAT_CELLPHONE_USR, EXCEP_FORMAT_CELLPHONE_DEV, 400);
        }
        return true;
    }
    
}
