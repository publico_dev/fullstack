package com.tcs.api.core.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tcs.api.core.model.Funciones;

/**
 * Repository de Spring para las operaciones CRUD sobre la tabla funciones
 * 
 * @author omar.cardona
 *
 * @version 1.0
 *
 */
@Repository
public interface FuncionesRepository extends JpaRepository<Funciones, Long> {

	/**
	 * Permite consultar una Funcion mediante su id
	 * 
	 * @param id Identificador de Funciones a consultar
	 * @return Objecto con la información de Funciones en caso de que exista un resultado
	 */
	Optional<Funciones> findById(Integer id);
	
}
