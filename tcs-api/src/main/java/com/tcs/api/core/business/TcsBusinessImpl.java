package com.tcs.api.core.business;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.tcs.api.core.domain.EmpleadoDTO;
import com.tcs.api.core.domain.response.Response;
import com.tcs.api.core.exception.CoreException;
import com.tcs.api.core.mapper.DepartamentoMapper;
import com.tcs.api.core.mapper.EmpleadoMapper;
import com.tcs.api.core.mapper.FuncionesMapper;
import com.tcs.api.core.model.Departamento;
import com.tcs.api.core.model.Empleado;
import com.tcs.api.core.model.Funciones;
import com.tcs.api.core.repository.DepartamentoRepository;
import com.tcs.api.core.repository.EmpleadoRepository;
import com.tcs.api.core.repository.FuncionesRepository;
import com.tcs.api.core.util.UtilConstants;
import com.tcs.api.core.util.ValidatorUtil;

/**
 * Clase encargada de implementar la logica de negocios
 * 
 * @author omar.cardona
 *
 * @version 1.0
 */
@Service
public class TcsBusinessImpl implements TcsBusiness {
	/** Logger */
    private static final Logger logger = LoggerFactory.getLogger(TcsBusinessImpl.class);
    
    
	/** Clase de repositorio de Departamento */
    private DepartamentoRepository repDepartamento;
	
    /** Clase de repositorio de Funciones */
    private FuncionesRepository repFunciones;
    
	/** Clase de repositorio de Empleado */
    private EmpleadoRepository repEmpleado;
    
    
    /** Clase de mapeo para Departamento */
    private DepartamentoMapper mapDepartamento;
    
    /** Clase de mapeo para Funciones */
    private FuncionesMapper mapFunciones;
    
    /** Clase de mapeo para Empleado */
    private EmpleadoMapper mapEmpleado;
    
    /**
     * Constructor
     */
    public TcsBusinessImpl(DepartamentoRepository repDepartamento, FuncionesRepository repFunciones, EmpleadoRepository repEmpleado, 
    		DepartamentoMapper mapDepartamento, FuncionesMapper mapFunciones, EmpleadoMapper mapEmpleado) {
    	this.repDepartamento = repDepartamento;
    	this.repFunciones = repFunciones;
    	this.repEmpleado = repEmpleado;
    	
    	this.mapDepartamento = mapDepartamento;
    	this.mapFunciones = mapFunciones;
    	this.mapEmpleado = mapEmpleado;
    }
    
    /**
     * @see TcsBusiness#empleado(EmpleadoDTO)
     */
	@Override
	public Response<Void> empleado(String numeroDocumento) {
		logger.debug("Start empleado: {}", numeroDocumento);
		Response<Void> response = new Response<>();
        
        try {
        	ValidatorUtil.isNullOrEmpty(numeroDocumento);
        	
        	Empleado empleado = empleadoFindByNumeroDocumento(numeroDocumento);
        	
        	response.setStatus(200);
            response.setUserMessage("Consulta de empleado exitosa.");
            response.setDeveloperMessage(UtilConstants.MSG_TRANSACTION_OK);
            response.setData(mapEmpleado.fromEntityToDto(empleado));
            
        } catch (Exception e) {
            logger.error("ERROR", e);
            response.setStatus(500);
            response.setUserMessage(UtilConstants.MSG_TRANSACTION_ERROR);
            response.setDeveloperMessage(e.getMessage());
            response.setErrorCode("0001");
        } finally {
        	response.setMoreInfo(UtilConstants.LINK_MORE_INFO);
        }

        logger.debug("End empleado");
        
        return response;
	}
	
	/**
     * @see TcsBusiness#empleadoList(int, int, String, String)
     */
	@Override
	public Response<Void> empleadoList(int page, int pageSize, String sort, String sortDirection) {
		logger.debug("Start empleadoList");
		Response<Void> response = new Response<>();
        
        try {
        	// Paginación de la consulta
        	Direction direction = Sort.Direction.DESC;
        	if( sortDirection.equalsIgnoreCase("ASC"))
        		direction = Sort.Direction.ASC;
        		
			Pageable pageable = PageRequest.of(page, pageSize, Sort.by(direction, sort));
			
			Page<Empleado> empleadosPage = repEmpleado.findAll(pageable);
			
			List<Empleado> empleadosList = empleadosPage.getContent();
			
        	response.setStatus(200);
            response.setUserMessage("Consulta de empleados exitosa.");
            response.setDeveloperMessage(UtilConstants.MSG_TRANSACTION_OK);
            response.setData(mapEmpleado.fromEntitiesToDtos(empleadosList));
            
        } catch (Exception e) {
            logger.error("ERROR", e);
            response.setStatus(500);
            response.setUserMessage(UtilConstants.MSG_TRANSACTION_ERROR);
            response.setDeveloperMessage(e.getMessage());
            response.setErrorCode("0001");
        } finally {
        	response.setMoreInfo(UtilConstants.LINK_MORE_INFO);
        }

        logger.debug("End empleadoList");
        
        return response;
	}
	
    /**
     * @see TcsBusiness#empleadoCreate(EmpleadoDTO)
     */
	@Override
	public Response<Void> empleadoCreate(EmpleadoDTO empleadoDTO) {
		logger.debug("Start empleadoCreate: {}", empleadoDTO);
        Response<Void> response = new Response<>();
        
        try {
        	ValidatorUtil.verifyEmpleadoDTO(empleadoDTO);
        	
        	Departamento departamento = departamentoFindByCode(empleadoDTO.getEmpDepartamento().getDepCodigo());
        	
        	Empleado empleado = new Empleado();
        	empleado = mapEmpleado.fromDtoToEntity(empleadoDTO);
        	empleado.setDepartamento(departamento);
        	empleado = repEmpleado.save(empleado);
        	
        	response.setStatus(200);
            response.setUserMessage("Creación de empleado exitosa.");
            response.setDeveloperMessage(UtilConstants.MSG_TRANSACTION_OK);
            response.setData(mapEmpleado.fromEntityToDto(empleado));
            
        } catch (Exception e) {
            logger.error("ERROR {}", e);
            response.setStatus(500);
            response.setUserMessage(UtilConstants.MSG_TRANSACTION_ERROR);
            response.setDeveloperMessage(e.getMessage());
            response.setErrorCode("0001");
        } finally {
        	response.setMoreInfo(UtilConstants.LINK_MORE_INFO);
        }

        logger.debug("End empleadoCreate");
        
        return response;
	}
    
	/**
     * @see TcsBusiness#empleadoUpdate(EmpleadoDTO)
     */
	@Override
	public Response<Void> empleadoUpdate(EmpleadoDTO empleadoDTO) {
		logger.debug("Start empleadoUpdate: {}", empleadoDTO);
        Response<Void> response = new Response<>();
        
        try {
        	ValidatorUtil.verifyEmpleadoDTOUpdate(empleadoDTO);
        	
        	Empleado empleado = empleadoFindByNumeroDocumento(empleadoDTO.getEmpNumeroDocumento());
        	
        	// Actualización de nombre
        	if( !ValidatorUtil.isObjectNull(empleadoDTO.getEmpNombre()) )
        		empleado.setNombre(empleadoDTO.getEmpNombre());
        	
        	// Actualización de apellidos
        	if( !ValidatorUtil.isNullOrEmpty(empleadoDTO.getEmpApellidos()) )
        		empleado.setApellidos(empleadoDTO.getEmpApellidos());
        	
        	// Actualización de correo
        	if( !ValidatorUtil.isNullOrEmpty(empleadoDTO.getEmpCorreo()) )
        		empleado.setCorreo(empleadoDTO.getEmpCorreo());
        	
        	// Actualización de telefono
        	if( !ValidatorUtil.isNullOrEmpty(empleadoDTO.getEmpTelefono()) )
        		empleado.setTelefono(empleadoDTO.getEmpTelefono());
        	
        	// Actualización de activo
        	if( !ValidatorUtil.isObjectNull(empleadoDTO.getEmpActivo()) )
        		empleado.setActivo(empleadoDTO.getEmpActivo());
        	
        	// Actualización de salario
        	if( !ValidatorUtil.isObjectNull(empleadoDTO.getEmpSalario()) )
        		empleado.setSalario(empleadoDTO.getEmpSalario());
        	
        	// Actualización de departamento
        	if( !ValidatorUtil.isObjectNull(empleadoDTO.getEmpDepartamento()) )
        		empleado.setDepartamento(departamentoFindByCode(empleadoDTO.getEmpDepartamento().getDepCodigo()));
        	
        	empleado = repEmpleado.save(empleado);
        	
        	response.setStatus(200);
            response.setUserMessage("Actualización de empleado exitosa.");
            response.setDeveloperMessage(UtilConstants.MSG_TRANSACTION_OK);
            response.setData(mapEmpleado.fromEntityToDto(empleado));
            
        } catch (Exception e) {
            logger.error("ERROR {}", e);
            response.setStatus(500);
            response.setUserMessage(UtilConstants.MSG_TRANSACTION_ERROR);
            response.setDeveloperMessage(e.getMessage());
            response.setErrorCode("0001");
        } finally {
        	response.setMoreInfo(UtilConstants.LINK_MORE_INFO);
        }

        logger.debug("End empleadoUpdate");
        
        return response;
	}
	
	/**
	 * Método que permite consultar un empleado por número de documento
	 * @param codigo Código del departamento
	 * @return Entidad de tipo Departamento, en caso de que exista
	 * @throws CoreException 
	 */
    private Empleado empleadoFindByNumeroDocumento(String numeroDocumento) throws CoreException {
    	Optional<Empleado> empleadoOpt = repEmpleado.findByNumeroDocumento(numeroDocumento);
    	
    	Empleado empleado = null;
    	if(empleadoOpt.isPresent()) {
    		empleado = empleadoOpt.get();
    	} else {
    		throw new CoreException("Empleado no existe", UtilConstants.MSG_TRANSACTION_ERROR, 400);
    	}
    	return empleado;
    }
    
	/**
	 * Método que permite consultar un departamento por código
	 * @param codigo Código del departamento
	 * @return Entidad de tipo Departamento, en caso de que exista
	 * @throws CoreException 
	 */
    private Departamento departamentoFindByCode(Integer codigo) throws CoreException {
    	Optional<Departamento> departamentoOpt = repDepartamento.findByCodigo(codigo);
    	
    	Departamento departamento = null;
    	if(departamentoOpt.isPresent()) {
    		departamento = departamentoOpt.get();
    	} else {
    		throw new CoreException("Departamento no existe", UtilConstants.MSG_TRANSACTION_ERROR, 400);
    	}
    	return departamento;
    }
    
    /**
     * @see TcsBusiness#departamentoList(int, int, String, String)
     */
	@Override
	public Response<Void> departamentoList(int page, int pageSize, String sort, String sortDirection) {
		logger.debug("Start departamentoList");
		Response<Void> response = new Response<>();
        
        try {
        	// Paginación de la consulta
        	Direction direction = Sort.Direction.DESC;
        	if( sortDirection.equalsIgnoreCase("ASC"))
        		direction = Sort.Direction.ASC;
        		
			Pageable pageable = PageRequest.of(page, pageSize, Sort.by(direction, sort));
			
			Page<Departamento> departamentosPage = repDepartamento.findAll(pageable);
			
			List<Departamento> empleadosList = departamentosPage.getContent();
			
        	response.setStatus(200);
            response.setUserMessage("Consulta de departamentos exitosa.");
            response.setDeveloperMessage(UtilConstants.MSG_TRANSACTION_OK);
            response.setData(mapDepartamento.fromEntitiesToDtos(empleadosList));
            
        } catch (Exception e) {
            logger.error("ERROR", e);
            response.setStatus(500);
            response.setUserMessage(UtilConstants.MSG_TRANSACTION_ERROR);
            response.setDeveloperMessage(e.getMessage());
            response.setErrorCode("0001");
        } finally {
        	response.setMoreInfo(UtilConstants.LINK_MORE_INFO);
        }

        logger.debug("End departamentoList");
        
        return response;
	}
	
	/**
     * @see TcsBusiness#departamentoReport(int, int, String, String)
     */
	@Override
	public Response<Void> departamentoReport() {
		logger.debug("Start departamentoReport");
		Response<Void> response = new Response<>();
        
        try {
        				
			List<Empleado> empleadosRepSal = repEmpleado.findAvgSalario();
			
        	response.setStatus(200);
            response.setUserMessage("Consulta de reporte exitosa.");
            response.setDeveloperMessage(UtilConstants.MSG_TRANSACTION_OK);
            response.setData(mapEmpleado.fromEntitiesToReport(empleadosRepSal));
            
        } catch (Exception e) {
            logger.error("ERROR", e);
            response.setStatus(500);
            response.setUserMessage(UtilConstants.MSG_TRANSACTION_ERROR);
            response.setDeveloperMessage(e.getMessage());
            response.setErrorCode("0001");
        } finally {
        	response.setMoreInfo(UtilConstants.LINK_MORE_INFO);
        }

        logger.debug("End departamentoReport");
        
        return response;
	}
	
	/**
     * @see TcsBusiness#funcionesList(int, int, String, String)
     */
	@Override
	public Response<Void> funcionesList(int page, int pageSize, String sort, String sortDirection) {
		logger.debug("Start funcionesList");
		Response<Void> response = new Response<>();
        
        try {
        	// Paginación de la consulta
        	Direction direction = Sort.Direction.DESC;
        	if( sortDirection.equalsIgnoreCase("ASC"))
        		direction = Sort.Direction.ASC;
        		
			Pageable pageable = PageRequest.of(page, pageSize, Sort.by(direction, sort));
			
			Page<Funciones> departamentosPage = repFunciones.findAll(pageable);
			
			List<Funciones> empleadosList = departamentosPage.getContent();
			
        	response.setStatus(200);
            response.setUserMessage("Consulta de funciones exitosa.");
            response.setDeveloperMessage(UtilConstants.MSG_TRANSACTION_OK);
            response.setData(mapFunciones.fromEntitiesToDtos(empleadosList));
            
        } catch (Exception e) {
            logger.error("ERROR", e);
            response.setStatus(500);
            response.setUserMessage(UtilConstants.MSG_TRANSACTION_ERROR);
            response.setDeveloperMessage(e.getMessage());
            response.setErrorCode("0001");
        } finally {
        	response.setMoreInfo(UtilConstants.LINK_MORE_INFO);
        }

        logger.debug("End funcionesList");
        
        return response;
	}
    
}
