package com.tcs.api.core.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.tcs.api.core.model.Empleado;

/**
 * Repository de Spring para las operaciones CRUD sobre la tabla empleado
 * 
 * @author omar.cardona
 *
 * @version 1.0
 *
 */
@Repository
public interface EmpleadoRepository extends JpaRepository<Empleado, Long> {

	/**
	 * Permite consultar un Empleado mediante su id
	 * 
	 * @param id Identificador de Empleado a consultar
	 * @return Objecto con la informaci�n de Empleado en caso de que exista un resultado
	 */
	Optional<Empleado> findById(Integer id);
	
	/**
	 * Permite consultar un Empleado mediante su n�mero de documento
	 * 
	 * @param numeroDocumento N�mero de documento de Empleado a consultar
	 * @return Objecto con la informaci�n de Empleado en caso de que exista un resultado
	 */
	Optional<Empleado> findByNumeroDocumento(String numeroDocumento);
	
	/**
	 * Permite consultar el reporte de promediosalarial por departamento
	 * 
	 * @return Objecto con la informaci�n del reporte
	 */
	@Query(value="SELECT id, activo, apellidos, correo, nombre, numero_documento, AVG(salario) AS salario, telefono, departamento_id FROM Empleado GROUP BY departamento_id", nativeQuery = true)
	List<Empleado> findAvgSalario();
	
}
