package com.tcs.api.core.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Clase que define una entidad para el mapeo O/R de la tabla departamento
 * 
 * @author omar.cardona
 *
 * @version 1.0
 * 
 */
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "nombre" }))
@Entity
public class Departamento {

	/** Id de la entidad */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	/** Nombre del departamento */
	private String nombre;
	
	/** Descripción del departamento */
	private String descripcion;
	
	/** Código del departamento */
	private Integer codigo;

	/** Funciones que tiene el departamento */
	@OneToMany(mappedBy = "departamento", cascade = CascadeType.ALL)
    private Set<Funciones> funciones;
	
	/** Empleados que tiene el departamento */
	@OneToMany(mappedBy = "departamento", cascade = CascadeType.ALL)
    private Set<Empleado> empleados;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the codigo
	 */
	public Integer getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the funciones
	 */
	public Set<Funciones> getFunciones() {
		return funciones;
	}

	/**
	 * @param funciones the funciones to set
	 */
	public void setFunciones(Set<Funciones> funciones) {
		this.funciones = funciones;
	}
	

	/**
	 * @return the empleados
	 */
	public Set<Empleado> getEmpleados() {
		return empleados;
	}

	/**
	 * @param empleados the empleados to set
	 */
	public void setEmpleados(Set<Empleado> empleados) {
		this.empleados = empleados;
	}

	@Override
	public String toString() {
		return "Departamento [id=" + id + ", nombre=" + nombre + ", descripcion=" + descripcion + ", codigo=" + codigo
				+ ", funciones=" + funciones.toString() + ", empleados=" + empleados.toString() + "]";
	}

}
