package com.tcs.api.core.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Clase que define una entidad para el mapeo O/R de la tabla funciones
 * 
 * @author omar.cardona
 *
 * @version 1.0
 * 
 */
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "nombre" }))
@Entity
public class Funciones {

	/** Id de la entidad */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	/** Nombre de la funci�n */
	private String nombre;
	
	/** Descripci�n de la funci�n */
	private String descripcion;
	
	/** Departamento al cual pertenece la funci�n */
	@ManyToOne
    @JoinColumn
    private Departamento departamento;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the departamento
	 */
	public Departamento getDepartamento() {
		return departamento;
	}

	/**
	 * @param departamento the departamento to set
	 */
	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

	@Override
	public String toString() {
		return "Funciones [id=" + id + ", nombre=" + nombre + ", descripcion=" + descripcion + ", departamento="
				+ departamento.toString() + "]";
	}

}
