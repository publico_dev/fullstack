package com.tcs.api.core.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tcs.api.core.model.Departamento;

/**
 * Repository de Spring para las operaciones CRUD sobre la tabla departamento
 * 
 * @author omar.cardona
 *
 * @version 1.0
 *
 */
@Repository
public interface DepartamentoRepository extends JpaRepository<Departamento, Integer> {

	/**
	 * Permite consultar un Departamento mediante su id
	 * 
	 * @param id Identificador de Departamento a consultar
	 * @return Objecto con la informaci�n de Departamento en caso de que exista un resultado
	 */
	Optional<Departamento> findById(Integer id);
	
	/**
	 * Permite consultar un Departamento mediante su c�digo
	 * 
	 * @param codigo Identificador de Departamento a consultar
	 * @return Objecto con la informaci�n de Departamento en caso de que exista un resultado
	 */
	Optional<Departamento> findByCodigo(Integer codigo);
	
}
