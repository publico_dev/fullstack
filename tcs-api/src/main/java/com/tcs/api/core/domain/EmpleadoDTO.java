package com.tcs.api.core.domain;

import java.io.Serializable;

/**
 * Clase que define un DTO para la transmisi�n de la informaci�n de entidades Empleado
 * 
 * @author omar.cardona
 * 
 * @version 1.0
 */
public class EmpleadoDTO implements Serializable {

    /** Serializes */
	private static final long serialVersionUID = -7208686037869945048L;

	/** Identificador del empleado */
	private Integer empId;
	
	/** Nombres del empleado */
	private String empNombre;
	
	/** Apellidos del empleado */
	private String empApellidos;
	
	/** n�mero de documento del empleado */
	private String empNumeroDocumento;
	
	/** Correo electr�nico del empleado */
	private String empCorreo;
	
	/** Tel�fono del empleado */
	private String empTelefono;
	
	/** Indica el estado del empleado en la plataforma */
	private boolean empActivo;
	
	/** Salario del empleado */
	private Double empSalario;
	
	/** Departamento al cual pertenece el empleado */
    private DepartamentoDTO empDepartamento;

	/**
	 * @return the empId
	 */
	public Integer getEmpId() {
		return empId;
	}

	/**
	 * @param empId the empId to set
	 */
	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	/**
	 * @return the empNombre
	 */
	public String getEmpNombre() {
		return empNombre;
	}

	/**
	 * @param empNombre the empNombre to set
	 */
	public void setEmpNombre(String empNombre) {
		this.empNombre = empNombre;
	}

	/**
	 * @return the empApellidos
	 */
	public String getEmpApellidos() {
		return empApellidos;
	}

	/**
	 * @param empApellidos the emppellidos to set
	 */
	public void setEmpApellidos(String empApellidos) {
		this.empApellidos = empApellidos;
	}

	/**
	 * @return the empNumeroDocumento
	 */
	public String getEmpNumeroDocumento() {
		return empNumeroDocumento;
	}

	/**
	 * @param empNumeroDocumento the empNumeroDocumento to set
	 */
	public void setEmpNumeroDocumento(String empNumeroDocumento) {
		this.empNumeroDocumento = empNumeroDocumento;
	}

	/**
	 * @return the empCorreo
	 */
	public String getEmpCorreo() {
		return empCorreo;
	}

	/**
	 * @param empCorreo the empCorreo to set
	 */
	public void setEmpCorreo(String empCorreo) {
		this.empCorreo = empCorreo;
	}

	/**
	 * @return the empTelefono
	 */
	public String getEmpTelefono() {
		return empTelefono;
	}

	/**
	 * @param empTelefono the empTelefono to set
	 */
	public void setEmpTelefono(String empTelefono) {
		this.empTelefono = empTelefono;
	}

	/**
	 * @return the empActivo
	 */
	public boolean getEmpActivo() {
		return empActivo;
	}

	/**
	 * @param empActivo the empActivo to set
	 */
	public void setEmpActivo(boolean empActivo) {
		this.empActivo = empActivo;
	}

	/**
	 * @return the empSalario
	 */
	public Double getEmpSalario() {
		return empSalario;
	}

	/**
	 * @param empSalario the empSalario to set
	 */
	public void setEmpSalario(Double empSalario) {
		this.empSalario = empSalario;
	}

	/**
	 * @return the empDepartamento
	 */
	public DepartamentoDTO getEmpDepartamento() {
		return empDepartamento;
	}

	/**
	 * @param empDepartamento the empDepartamento to set
	 */
	public void setEmpDepartamento(DepartamentoDTO empDepartamento) {
		this.empDepartamento = empDepartamento;
	}

	@Override
	public String toString() {
		return "EmpleadoDTO [empId=" + empId + ", empNombre=" + empNombre + ", empApellidos=" + empApellidos
				+ ", empNumeroDocumento=" + empNumeroDocumento + ", empCorreo=" + empCorreo + ", empTelefono="
				+ empTelefono + ", empActivo=" + empActivo + ", empSalario=" + empSalario + ", empDepartamento="
				+ empDepartamento + "]";
	}
    
}
