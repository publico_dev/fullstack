package com.tcs.api.core.exception;

/**
 * Permite controlar excepciones propias de la aplicacion
 * 
 * @author omar.cardona
 *
 * @version 1.0
 * 
 */
public class CoreException extends Exception {

	/** Serialize */
	private static final long serialVersionUID = 6365652257268547172L;
	/** Mensaje informativo para el usuario */
	private final String userMessage;
	/** Codigo que define el estado de la transaccion */
	private final int status;

	/**
	 * M�todo constructor
	 */
	public CoreException(String developerMessage, String userMessage, int status, Throwable e) {
		super(developerMessage, e);
		this.userMessage = userMessage;
		this.status = status;
	}

	/**
	 * M�todo constructor
	 */
	public CoreException(String developerMessage, String userMessage, int status) {
		super(developerMessage);
		this.userMessage = userMessage;
		this.status = status;
	}

	/**
	 * M�todo constructor
	 */
	public CoreException(String userMessage, int state, Throwable e) {
		super(e);
		this.userMessage = userMessage;
		this.status = state;
	}

	/**
	 * @return the userMessage
	 */
	public String getUserMessage() {
		return userMessage;
	}

	/**
	 * @return the state
	 */
	public int getState() {
		return status;
	}

}
