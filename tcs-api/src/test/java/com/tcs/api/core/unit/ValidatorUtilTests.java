package com.tcs.api.core.unit;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.tcs.api.core.domain.DepartamentoDTO;
import com.tcs.api.core.domain.EmpleadoDTO;
import com.tcs.api.core.exception.CoreException;
import com.tcs.api.core.util.ValidatorUtil;

/**
 * Clase que realiza las pruebas unitarias sobre los metodos de la clase
 * {@link ValidatorUtil}
 * 
 * @author omar.cardona@pragma.com.co
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(value = "dev")
public class ValidatorUtilTests {
	
	/** Datos para la ejecucion de pruebas */
	private static final String TEST_MESS_EXCEP_VERIFY = "Los datos son inv�lidos, por favor verifique.";
	
	@Value("${test.user-document}")
	private String TEST_USER_DOC;
	
	@Rule
    public ExpectedException exception = ExpectedException.none();

	/**
	 * Prueba unitaria para comprobar el resultado de la validacion de un objeto
	 * nulo.
	 */
	@Test
	public void testIsObjectNull() {
		assertTrue(ValidatorUtil.isObjectNull(null));
	}

	/**
	 * Prueba unitaria para comprobar el resultado de la validacion de un objeto no
	 * nulo
	 */
	@Test
	public void testIsObjectNotNull() {
		assertFalse(ValidatorUtil.isObjectNull(new EmpleadoDTO()));
	}

	/**
	 * Prueba unitaria para comprobar la validacion de una cadena vacia
	 */
	@Test
	public void testIsEmptyString() {
		assertTrue(ValidatorUtil.isNullOrEmpty(""));
	}

	/**
	 * Prueba unitaria para comprobar la validacion de una cadena con informacion
	 */
	@Test
	public void testIsNotEmptyString() {
		assertFalse(ValidatorUtil.isNullOrEmpty("test"));
	}

	/**
	 * Prueba unitaria para comprobar la validacion de una cadena nula
	 */
	@Test
	public void testIsNullString() {
		assertTrue(ValidatorUtil.isNullOrEmpty(null));
	}
	
	/**
	 * Prueba unitaria para comprobar un RequestDTO v�lido
	 */
	@Test
	public void testVerifyRequestInfoValid() throws CoreException {
		DepartamentoDTO empDepartamento = new DepartamentoDTO();
    	empDepartamento.setDepCodigo(1001);
    	
		EmpleadoDTO empleadoDTO = new EmpleadoDTO();
		empleadoDTO.setEmpActivo(true);
		empleadoDTO.setEmpApellidos("Perez");
		empleadoDTO.setEmpCorreo("correo@correo.co");
		empleadoDTO.setEmpDepartamento(empDepartamento);
		empleadoDTO.setEmpNombre("Pepito");
		empleadoDTO.setEmpNumeroDocumento(TEST_USER_DOC);
		empleadoDTO.setEmpSalario(6500000.01);
		empleadoDTO.setEmpTelefono("3137543700");
		
		assertTrue(ValidatorUtil.verifyEmpleadoDTO(empleadoDTO));
	}
	
	/**
	 * Prueba unitaria para comprobar un RequestDTO inválido
	 */
	@Test
	public void testVerifyRequestInfoInvalid1() throws CoreException {
		exception.expect(CoreException.class);
        exception.expectMessage(TEST_MESS_EXCEP_VERIFY);
        
        EmpleadoDTO empleadoDTO = new EmpleadoDTO();
		
		ValidatorUtil.verifyEmpleadoDTO(empleadoDTO);
	}
	
}
