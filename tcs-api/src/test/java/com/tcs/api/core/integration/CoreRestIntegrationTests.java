package com.tcs.api.core.integration;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.tcs.api.core.domain.DepartamentoDTO;
import com.tcs.api.core.domain.EmpleadoDTO;
import com.tcs.api.core.domain.response.Response;
import com.tcs.api.core.rest.CoreRest;

import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 * Clase que permite ejecutar las pruebas de integracion sobre los m�todos de negocio de la clase {@link CoreRest}
 * 
 * @author omar.cardona
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@FixMethodOrder(MethodSorters.NAME_ASCENDING) // Orden de ejecuci�n del test
public class CoreRestIntegrationTests {
	
	/** Datos para la ejecucion de pruebas */
	EmpleadoDTO empleadoDTO;
	
	@Value("${test.user-document}")
	private String TEST_USER_DOC;
	
	@Value("${test.page}")
	private int TEST_PAGE;
	
	@Value("${test.page-size}")
	private int TEST_PAGE_SIZE;
	
	@Value("${test.sort}")
	private String TEST_SORT;
	
	@Value("${test.sort-direction}")
	private String TEST_SORT_DIR;
	
	@Autowired
    CoreRest coreRest;

	/**
	 * Constructor
	 */
    public CoreRestIntegrationTests() {
    	System.setProperty("spring.profiles.active","dev"); // Configuraci�n de perfil DEV
	}

	/**
     * Permite inicializar datos requeridos para las pruebas
     */
    @Before
    public void initTest() {
    	DepartamentoDTO empDepartamento = new DepartamentoDTO();
    	empDepartamento.setDepCodigo(1001);
    	
		empleadoDTO = new EmpleadoDTO();
		empleadoDTO.setEmpActivo(true);
		empleadoDTO.setEmpApellidos("Perez");
		empleadoDTO.setEmpCorreo("correo@correo.co");
		empleadoDTO.setEmpDepartamento(empDepartamento);
		empleadoDTO.setEmpNombre("Pepito");
		empleadoDTO.setEmpNumeroDocumento(TEST_USER_DOC);
		empleadoDTO.setEmpSalario(6500000.01);
		empleadoDTO.setEmpTelefono("3137543700");
    }

    /**
     * Prueba de integracion para la consulta de empleado
     */
    @Test
    public void test001empleadoFail() {
    	
		Response<Void> response = coreRest.empleado(TEST_USER_DOC);
		
        assertThat(response.getStatus()).isEqualTo(500);
        assertThat(response.getErrorCode()).contains("0001");
    }
    
    /**
     * Prueba de integracion para la consulta de empleados
     */
    @Test
    public void test002empleadoListOk() {
		Response<Void> response = coreRest.empleadoList(TEST_PAGE, TEST_PAGE_SIZE, TEST_SORT, TEST_SORT_DIR);
		
        assertThat(response.getStatus()).isEqualTo(200);
        assertThat(response.getErrorCode()).isBlank();
    }
    
    /**
     * Prueba de integracion para la creaci�n de empleado
     */
    @Test
    public void test003empleadoCreateFail() {
		empleadoDTO.setEmpCorreo("correocorreo.co");
		
		Response<Void> response = coreRest.empleadoCreate(empleadoDTO);
		
        assertThat(response.getStatus()).isEqualTo(500);
        assertThat(response.getDeveloperMessage()).contains("no cumple con el formato");
    }
    
    
}
