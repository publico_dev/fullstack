﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Subscription, throwError } from 'rxjs';
import { Router} from '@angular/router';

import { environment } from '@environments/environment';
import { RespGral, Empleado } from '@app/_models';

@Injectable({ providedIn: 'root' })
export class TcsService {
  currentUserSubscription: Subscription;
  constructor(
    private http: HttpClient,
    private router: Router,
  ) {

  }


  // DEPARTAMENTO
  departamentoGetAll() {
    return this.http.get(`${environment.apiUrl}/departamento/list?page=0&pageSize=100&sort=nombre&sortDirection=ASC`).
      pipe(
        map((data: RespGral) => {
          return data;
        }), catchError(error => {
          return throwError('Error en departamentoGetAll');
        })
      )
  }

  departamentoGetReport() {
    return this.http.get(`${environment.apiUrl}/departamento/report`).
      pipe(
        map((data: RespGral) => {
          return data;
        }), catchError(error => {
          return throwError('Error en departamentoGetReport');
        })
      )
  }


  // EMPLEADO
  empleadoGetAll() {
    return this.http.get<RespGral>(`${environment.apiUrl}/empleado/list?page=0&pageSize=100&sort=apellidos&sortDirection=ASC`);
  }

  empleadoAdd(empleado: Empleado) {
    return this.http.post<RespGral>(`${environment.apiUrl}/empleado/create`, empleado);
  }

  empleadoEdit(empleado: Empleado) {
    return this.http.put<RespGral>(`${environment.apiUrl}/empleado/update`, empleado)
  }










}