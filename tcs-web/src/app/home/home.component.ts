﻿import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import { environment } from '@environments/environment';

import { Empleado } from '@app/_models';
import { TcsService } from '@app/_services';

@Component({ templateUrl: 'home.component.html' })
export class HomeComponent implements OnInit, OnDestroy {
    appName1: string;
    appName2: string;

    currentUser: Empleado;
    currentUserSubscription: Subscription;
    empleados: Empleado[] = [];

    constructor(
        private userService: TcsService
    ) {
        this.appName1 = `${environment.appName1}`;
        this.appName2 = `${environment.appName2}`;
    }

    ngOnInit() {
    }

    ngOnDestroy() {
    }

}