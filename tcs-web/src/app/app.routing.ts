﻿import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home';
import { DepartamentoComponent } from './departamento';
import { EmpleadoComponent } from './empleado';

const appRoutes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'departamento', component: DepartamentoComponent },
    { path: 'empleado', component: EmpleadoComponent },

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);