﻿import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule }    from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { DndModule } from '@beyerleinf/ngx-dnd';
import { DataTablesModule } from 'angular-datatables';

import { AppComponent }  from './app.component';
import { routing }        from './app.routing';

import { AlertComponent } from './_components';
import { HomeComponent } from './home';
import { LoadingComponent } from './loading';
import { NavigationComponent } from './navigation/navigation.component';
import { DepartamentoComponent } from './departamento/departamento.component';
import { EmpleadoComponent } from './empleado/empleado.component';
import { LoadingIconComponent } from './loading-icon/loading-icon.component'

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        routing,
        DataTablesModule,
        DndModule.forRoot()
    ],
    declarations: [
        AppComponent,
        AlertComponent,
        HomeComponent,
        LoadingComponent,
        NavigationComponent,
        DepartamentoComponent,
        EmpleadoComponent,
        LoadingIconComponent
    ],
    providers: [
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }