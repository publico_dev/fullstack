import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { environment } from '@environments/environment';
import { Empleado, RespGral, Departamento } from '@app/_models';
import { TcsService, AlertService } from '@app/_services';


@Component({
  selector: 'app-empleado',
  templateUrl: './empleado.component.html',
  styleUrls: ['./empleado.component.css']
})
export class EmpleadoComponent implements OnInit {

  loading = false;
  listDepartamento: Departamento[];
  listEmpleado: Empleado[];
  listEmpleadoDeleted: Departamento[];
  empleadoSelected: Empleado;
  newEmpleado: Empleado = new Empleado();

  constructor(
    private tcsService: TcsService,
    private alertService: AlertService,
  ) { }

  ngOnInit() {
    this.loadAllEmpleado();
    this.loadAllDepartamento();
  }

  private loadAllDepartamento() {
    this.tcsService.departamentoGetAll().subscribe(
      (data:RespGral) => {
        this.listDepartamento = data.data;
      },
      error => {
        console.log("ERROR: ", error);
        this.alertService.error(error, true);
    });
  }

  private loadAllEmpleado() {
    this.loading = true;
    this.tcsService.empleadoGetAll().subscribe(
      (data:RespGral) => {
        this.listEmpleado = data.data;
        this.loading = false;
      },
      error => {
        console.log("ERROR: ", error);
        this.alertService.error(error, true);
        this.loading = false;
    });
  }

  private assignObj(item:Empleado){
    this.empleadoSelected = Object.assign(Empleado, item);
  }

  filterChanged(event){
    let indexSel = event.target.selectedIndex;
    this.empleadoSelected.empDepartamento = this.listDepartamento[indexSel];
  }

  private addEmpleado() {
    this.loading = true;
    this.tcsService.empleadoAdd(this.newEmpleado).subscribe(
      (data:RespGral) => {
        this.loading = false;
        if(data.status===200){
          this.newEmpleado = new Empleado();
          this.loadAllEmpleado();
        }
        this.alertService.success(data.developerMessage, true);
      },
      error => {
        console.log("ERROR: ", error);
        this.loading = false;
        this.alertService.error(error, true);
        this.loadAllEmpleado();
    });
  }

  private editEmpleado() {
    this.loading = true;
    this.tcsService.empleadoEdit(this.empleadoSelected).subscribe(
      (data:RespGral) => {
        this.loading = false;
        if(data.status===200){
          this.alertService.success(data.developerMessage, true);
          this.loadAllEmpleado();
        } else {
          this.alertService.error(data.developerMessage, true);
        }
      },
      error => {
        console.log("ERROR: ", error);
        this.loading = false;
        this.alertService.error(error, true);
        this.loadAllEmpleado();
    });
  }

  private validateEmpleado(empl:Empleado):boolean {
    if( empl!=null 
      && empl.empApellidos 
      && empl.empNombre 
      && empl.empNumeroDocumento
      && empl.empCorreo
      && empl.empTelefono
      && empl.empSalario
      && empl.empDepartamento) {
      return true;
    }
    return false;
  }

}
