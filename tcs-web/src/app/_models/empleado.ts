﻿import { Departamento } from '@app/_models';

export class Empleado {
    empId: number;
    empNombre: string;
    empApellidos: string;
    empNumeroDocumento: string;
    empCorreo: string;
    empTelefono: string;
    empActivo: boolean;
    empSalario: number;
    empDepartamento: Departamento;
}