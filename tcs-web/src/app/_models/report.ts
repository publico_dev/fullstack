import { Departamento } from '@app/_models';

export class Report {
    promedio: number;
    empDepartamento: Departamento;
}