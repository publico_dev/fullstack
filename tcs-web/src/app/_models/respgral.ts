export class RespGral {
    systemDate: string;
    status: number;
    userMessage: string;
    developerMessage: string;
    errorCode: string;
    moreInfo: string;
    data: any;
}