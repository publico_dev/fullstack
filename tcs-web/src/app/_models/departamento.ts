export class Departamento {
    depId: number;
    depNombre: string;
    depDescripcion: string;
    depCodigo: number;
}