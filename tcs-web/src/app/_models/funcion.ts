import { Departamento } from '@app/_models';

export class Funcion {
    funId: number;
    funNombre: string;
    funDescripcion: string;
    funDepartamento: Departamento;
}