import { Component, OnInit } from '@angular/core';
import { RespGral, Report } from '@app/_models';
import { TcsService, AlertService } from '@app/_services';

@Component({
  selector: 'app-departamento',
  templateUrl: './departamento.component.html',
  styleUrls: ['./departamento.component.css']
})
export class DepartamentoComponent implements OnInit {
  loading = false;
  listDepartamento: Report[];

  constructor(
    private tcsService: TcsService,
    private alertService: AlertService,
  ) { }

  ngOnInit() {
    this.loadAllDepartamentos();
  }

  private loadAllDepartamentos() {
    this.loading = true;
    this.tcsService.departamentoGetReport().subscribe(
      (data:RespGral) => {
        this.listDepartamento = data.data;
        this.loading = false;
      },
      error => {
        console.log("ERROR: ", error);
        this.alertService.error(error, true);
        this.loading = false;
    });
  }
}
