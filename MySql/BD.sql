-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.32-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para tcs_bd
DROP DATABASE IF EXISTS `tcs_bd`;
CREATE DATABASE IF NOT EXISTS `tcs_bd` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `tcs_bd`;

-- Volcando estructura para tabla tcs_bd.departamento
DROP TABLE IF EXISTS `departamento`;
CREATE TABLE IF NOT EXISTS `departamento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` int(11) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKut33464xvtpuomfl3eshl252` (`nombre`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla tcs_bd.departamento: 3 rows
/*!40000 ALTER TABLE `departamento` DISABLE KEYS */;
REPLACE INTO `departamento` (`id`, `codigo`, `descripcion`, `nombre`) VALUES
	(1, 1001, 'Sed porttitor lectus nibh. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Curabitur aliquet quam id dui posuere blandit.', 'Mercadeo'),
	(2, 1002, 'Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Proin eget tortor risus.', 'Recursos Humanos'),
	(3, 1003, 'Proin eget tortor risus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque in ipsum id orci porta dapibus.', 'Tesorería');
/*!40000 ALTER TABLE `departamento` ENABLE KEYS */;

-- Volcando estructura para tabla tcs_bd.empleado
DROP TABLE IF EXISTS `empleado`;
CREATE TABLE IF NOT EXISTS `empleado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activo` bit(1) DEFAULT NULL,
  `apellidos` varchar(255) DEFAULT NULL,
  `correo` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `numero_documento` varchar(255) DEFAULT NULL,
  `salario` double DEFAULT NULL,
  `telefono` varchar(255) DEFAULT NULL,
  `departamento_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKjx1jvxwixok4qgln8yac7h4va` (`numero_documento`),
  KEY `FKhdjjhohpyjsfta5g6p8b8e00i` (`departamento_id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla tcs_bd.empleado: 2 rows
/*!40000 ALTER TABLE `empleado` DISABLE KEYS */;
REPLACE INTO `empleado` (`id`, `activo`, `apellidos`, `correo`, `nombre`, `numero_documento`, `salario`, `telefono`, `departamento_id`) VALUES
	(15, b'0', 'Cardona', 'gust1610@gmail.com', 'Pepito', '1053781006', 6500000.2, '3137540000', 1),
	(16, b'0', 'Arjona', 'gust1610@gmail.com', 'Angel', '1053781001', 650000.2, '3137540000', 3),
	(17, b'0', 'Suárez', 'correo@gmail.com', 'Augusto', '1053781002', 65000.2, '3137540000', 2),
	(18, b'0', 'Soto', 'correo@gmail.co', 'Carmen', '1053809000', 1500000, '3137540000', 2);
/*!40000 ALTER TABLE `empleado` ENABLE KEYS */;

-- Volcando estructura para tabla tcs_bd.funciones
DROP TABLE IF EXISTS `funciones`;
CREATE TABLE IF NOT EXISTS `funciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `departamento_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKifjhpaifh7hxt7ytpsmi75xr` (`nombre`),
  KEY `FKa24s63fg5onxj8yjn1cxt4srg` (`departamento_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla tcs_bd.funciones: 9 rows
/*!40000 ALTER TABLE `funciones` DISABLE KEYS */;
REPLACE INTO `funciones` (`id`, `descripcion`, `nombre`, `departamento_id`) VALUES
	(1, 'Quisque velit nisi, pretium ut lacinia in, elementum id enim. Proin eget tortor risus. Sed porttitor lectus nibh.', 'Investigación', 1),
	(2, 'Nulla porttitor accumsan tincidunt. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Donec rutrum congue leo eget malesuada.', 'Planificación', 1),
	(3, 'Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Pellentesque in ipsum id orci porta dapibus.', 'Implementación', 1),
	(4, 'Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Nulla porttitor accumsan tincidunt.', 'Reclutamiento', 2),
	(5, 'Vivamus suscipit tortor eget felis porttitor volutpat. Sed porttitor lectus nibh. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Curabitur aliquet quam id dui posuere blandit.', 'Selección', 2),
	(6, 'Ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Proin eget tortor risus.', 'Formación', 2),
	(7, 'Esta función del Departamento de Tesorería implica realizar un seguimiento de la posición diaria de tesorería para anticiparse a las posibles desviaciones. También conlleva gestionar los flujos de cobro y pago de la empresa, y decidir a qué entidad financ', 'Control y planificación de la liquidez', 3),
	(8, 'Es una función muy importante dentro de esta área financiera de la empresa. Esta negociación tiene que servir para reducir gastos de los servicios diferentes a los asociados con la inversión de los excedentes y la financiación de la demanda financiera. Ta', 'Negociaciones bancarias', 3),
	(9, 'Esta función se ejecuta, por ejemplo, tomando las decisiones de inversión oportunas, negociando las mismas con distintas entidades. También informándose de las alternativas existentes en el mercado que garanticen a la empresa mejor rentabilidad, liquidez ', 'Gestión de las necesidades y excedentes a corto plazo', 3);
/*!40000 ALTER TABLE `funciones` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
